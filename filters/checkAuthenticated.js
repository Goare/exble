module.exports = function(req, res, next)
{
    if (req.session.logged === true)
    {
        next();
    }
    else
    {
        return res.redirect('/user/sign_in?redir='+req.url);
    }
};