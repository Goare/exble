var request = require('supertest');
var app = require('../app.js').app;
var boot = require('../app.js').boot;
var halt = require('../app.js').halt;

describe('APP', function(){
    before(function(){
        boot();
    });
    after(function(){
        halt();
    });
    describe('#GET / ', function(){
        it('should return 200', function(done){
            request(app)
                .get('/')
                .expect(200)
                .end(function(err, res) {
                    if (err) return done(err);
                    done();
                })
        })
    });
});

var assert = require('assert');
describe('Array', function() {
    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal(-1, [1,2,3].indexOf(5));
            assert.equal(-1, [1,2,3].indexOf(0));
        });
    });
});