'use strict';

module.exports = function(sequelize, DataTypes){
    var File = sequelize.define('File', {
        name: {
            type: DataTypes.STRING
        },
        hash: {
            type: DataTypes.STRING
        },
        key: {
            type: DataTypes.STRING
        },
        isPublic: {
            type: DataTypes.BOOLEAN
        },
        size: {
            type: DataTypes.INTEGER
        },
        mimeType: {
            type: DataTypes.STRING
        },
        namePrefix: {
            type: DataTypes.STRING
        }
    });
    return File;
};
