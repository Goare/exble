exports.controllers = {
    // for testing
    '/testErr': {
       get: {
           route: function(req, res, next)
           {
               next(new Error(JSON.stringify({code: 502, info: '502 test error'})));
           }
       }
    },
    '/twit': {
        get: {
            preRoutes : ['log'],
            postRoutes: ['log2'],
            route: function(req, res, next)
            {
                var User = req.app.get('models').User;
                var Article = req.app.get('models').Article;

                User.findAll({where: {id: 1},include: [{model: Article, where: {title: {$like: '%first%'}}}]})
                    .then(function(users){
                        console.log('xxxxxxxx', JSON.stringify(users));
                        res.locals.users = users;

                        next();
                    })
                    .catch(function(err){
                        next(err);
                    });
            }
        },
        post: {
            route: function(req, res, next)
            {
                var email = req.body.email;
                var password = req.body.password;

                req.app.get('models').User
                    .create({
                        email: email,
                        password: password
                    })
                    .then(function(){
                        console.log('xxxxxxxx', 'saved successfully');

                        res.redirect('/twit');
                    })
            }
        }
    },
    'android-test': {
        get : {
            route: function(req, res, next)
            {
                console.log('req.query: ', req.query);
                res.send('get exble android test fine.');
            }
        },
        post: {
            route: function(req, res, next)
            {
                var name = req.body.name;
                var age = req.body.age;
                var text = req.body.text;

                console.log(name, age, text);
                res.json({'status': 'success', info: "post exble android test fine."})
            }
        }
    },
    'guestbook': {
        get: {
            route: function(req, res, next)
            {
                var GuestBook = req.app.get('models').GuestBook;
                GuestBook.findAll({
                    order: [
                        ['createdAt', 'DESC']
                    ]
                }).then(function(messages){
                    res.locals.messages = messages;

                    next();
                });
            }
        },
        post: {
            route: function(req, res, next)
            {
                var GuestBook = req.app.get('models').GuestBook;
                var username = req.body.username;
                var email = req.body.email;
                var message = req.body.message;

                GuestBook.create({
                    username: username,
                    email: email,
                    message: message
                })
                    .then(function(){
                       res.redirect('/guestbook');
                    });
            }
        }
    },
    '/sessions': {
        post: {
            route: function(req, res, next)
            {
                return res.json(req.session);
            }
        }
    },

    // bamket start
    '/': {
        get:
        {
            preRoutes: ['checkLogged'],
            route: function(req, res, next)
            {
                next();
            }
        }
    },
    '~:username(\\w+)' : {
        get: {
            preRoutes: ['checkLogged'],
            postRoutes: [],
            route: function(req, res, next)
            {
                //res.send(req.params);
                res.render('user/profile');
            }
        }
    },
    'about': {
        get: {
            preRoutes: ['checkLogged'],
            route: function(req, res, next)
            {
                next();
            }
        }
    }
};
