'use strict';

module.exports = function(sequelize, DataTypes){
    var UserActivity = sequelize.define('UserActivity', {
        ipAddress: {
            type: DataTypes.STRING
        },
        activityType: {
            type: DataTypes.INTEGER
        }
    });
    return UserActivity;
};
