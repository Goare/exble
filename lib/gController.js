var config = require('../config.js');
var path = require('path');

var gController = function(router, path)
{
    this.path = path.replace(/^\//, '');

    this.preRoutes  = router.preRoutes || [];
    this.postRoutes = router.postRoutes || [];
    this.route      = router.route;

    this.renderView = function()
    {
        var self = this;
        return function(req, res, next)
        {
            var viewPath = (self.path === '') ? 'index' : self.path;
            res.render(viewPath+'.jade');
        }
    };

};

gController.prototype = {
    getPreRoutes    : function()
    {
        var preRoutes = [];
        this.preRoutes.forEach(function(route, index){
            preRoutes.push(require(path.join(config.rootPath, config.filtersFolder,  route)));
        });

        return preRoutes;
    },
    getPostRoutes   : function()
    {
        var postRoutes = [];
        this.postRoutes.forEach(function(route, index){
            postRoutes.push(require(path.join(config.rootPath, config.filtersFolder, route)));
        });

        return postRoutes;
    },
    getRoute        : function()
    {
        return this.route;
    }
};


module.exports = gController;
