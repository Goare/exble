var express = require('express');
var app = express();

var path            = require('path');
var http            = require('http');
var https           = require('https');
var bodyParser      = require('body-parser');
var cookieParser    = require('cookie-parser');
var compression     = require('compression');
var i18n            = require('i18n');
var session         = require('express-session');
var RedisStore      = require('connect-redis')(session);
var sassMiddleware  = require('node-sass-middleware');

var gRouter = require('./lib/gRouter2.js');

var config = require('./config.js');


app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

app.use(express.static(__dirname + '/public'));

app.use(compression());

// session
app.use(session({
    cookie: {maxAge: 15*60*1000}, // 15 minutes
    secret: 'session-secret',
    name: 'session-name',
    resave: true,
    saveUninitialized: false,
    rolling: true,
    store: new RedisStore({host: config.redis.host, port: config.redis.port, pass: config.redis.pass})
}));

// node-sass-middleware
if (config.env === 'development')
{
    app.use(sassMiddleware({
        src: __dirname + '/public',
        dest: __dirname + '/public',
        response: true
    }));
}

// body-parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cookieParser());

// i18n, works with cookie-parser to read 'lang' cookie value
i18n.configure({
    locales:['en', 'cn', 'jp', 'kr', 'tw'],
    directory: config.rootPath + '/locales',
    cookie: 'lang',
    updateFiles: false,
    objectNotation: true
});
app.use(i18n.init);

gRouter(app);

// 404
app.all('*', function(req, res, next){
   res.status(404).render('404.jade');
});

// error logs
app.use(function(err, req, res, next){
    console.log(err.stack);
    next(err);
});
// handle errors
app.use(function(err, req, res, next){
    var errMsg,
        errCode = 500,
        errInfo = '500 Server Error';
    try
    {
        errMsg = JSON.parse(err.message);

        errCode = errMsg.code || errCode;
        errInfo = errMsg.info || errInfo;
    }
    catch(e)
    {
        errInfo = err;
    }


    res.status(errCode).render(errCode+'.jade', {error: errInfo});

});

app.locals.moment = require('moment');

var models = require('./models');
app.set('models', models);

var httpServer = http.createServer(app);
// start
var boot = function() {
    app.get('models').sequelize.sync({force: false}).then(function(){

        // db seed
        //require(path.join(config.rootPath, 'db/seed.js'))(models);

        var port = config.port;
        var address = config.address;

        httpServer.listen(port, address);
        httpServer.on('listening', function(){
            console.log('Http server starts at address: ' + httpServer.address().address + ' , port: ' +httpServer.address().port);
        });
    })
};
var halt = function() {
  httpServer.close();
};

if (!module.parent)
{
    boot();
}
else
{
    console.log('app running as a module');
    exports.boot = boot;
    exports.halt = halt;
    exports.app  = app;
}
