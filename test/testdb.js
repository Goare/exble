var db = require('../models');
var expect = require('chai').expect;

describe('Model User', function(){
    describe('#findOne', function(){
        it('get one user object', function(done){
            db.User.findOne().then(function(user){
                expect(user.name).to.be.a('string');
                expect(user.password).to.be.a('string');
                expect(user.email).to.be.a('string');
                done();
            })
        })
    });
});
