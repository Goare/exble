'use strict';

module.exports = function(sequelize, DataTypes) {
    var Article = sequelize.define('Article', {
        title: {
            type: DataTypes.STRING
        },
        content: {
            type: DataTypes.STRING
        },
        category: {
            type: DataTypes.STRING
        },
        tag: {
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function(models) {
                Article.belongsTo(models.User)
            }
        }
    });

    return Article;
};