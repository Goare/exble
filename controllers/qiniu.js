var qiniu = require('qiniu');
var config = require('../config.js');

qiniu.conf.ACCESS_KEY = config.qiniu.ACCESS_KEY;
qiniu.conf.SECRET_KEY = config.qiniu.SECRET_KEY;

exports.controllers = {
    '/getPolicy': {
        post: {
            route: function(req, res, next) {
                var logged = req.session.logged;
                var userId = req.session.userId;
                var username = req.session.username;
                var bucketname,
                    putPolicy,
                    sizeLimit,
                    namePrefix;

                if (logged === true) {
                    bucketname = 'bamket-private';
                    putPolicy  = new qiniu.rs.PutPolicy(bucketname);
                    namePrefix = username;


                    putPolicy.callbackUrl = 'http://121.40.130.207:5678/qiniu/callbackAction';
                    putPolicy.callbackBody = 'key=$(key)&hash=$(etag)&size=$(fsize)&mimeType=$(mimeType)&filename=$(x:filename)&namePrefix=$(x:namePrefix)&logged=$(x:logged)&userId=$(x:userId)';
                    putPolicy.callbackHost = 'callbackBamket.xxx';

                }
                else
                {

                    bucketname = 'bamket-public';
                    putPolicy  = new qiniu.rs.PutPolicy(bucketname);
                    sizeLimit  = 1024 * 1024 * 10; // 10m
                    namePrefix = Date.now();

                    putPolicy.callbackUrl = 'http://121.40.130.207:5678/qiniu/callbackAction';
                    putPolicy.callbackBody = 'key=$(key)&hash=$(etag)&size=$(fsize)&mimeType=$(mimeType)&filename=$(x:filename)&namePrefix=$(x:namePrefix)&logged=$(x:logged)&userId=$(x:userId)';
                    putPolicy.callbackHost = 'callbackBamket.xxx';
                    //putPolicy.callbackBodyType = "application/json";
                    //putPolicy.returnUrl = returnUrl;
                    //putPolicy.returnBody = '{"key":"$(key)","hash":"$(etag)","size":"$(fsize)","mimeType":"$(mimeType)"}';
                    //putPolicy.asyncOps = asyncOps;
                    //putPolicy.expires = expires;
                    putPolicy.fsizeLimit = sizeLimit;


                }

                return res.json({
                    token: putPolicy.token(),
                    sizeLimit: sizeLimit,
                    namePrefix: namePrefix,
                    logged: logged,
                    userId: userId
                });
            }
        }
    },
    '/callbackAction': {
        'post': {
            route: function(req, res, next)
            {
                var callbackHost = req.hostname;
                if (callbackHost !== 'callbackBamket.xxx')
                {
                    return res.json({status: 0, info: 'hey request, where are you come from?'});
                }

                var key            = req.body.key;
                var hash           = req.body.hash;
                var size           = req.body.size;
                var mimeType       = req.body.mimeType;
                var name           = req.body.filename;
                var namePrefix     = req.body.namePrefix;
                var isPublic       = req.body.logged === 'true' ? 0 : 1;
                var userId         = parseInt(req.body.userId) || null;

                // activities


                var File = req.app.get('models').File;

                File.create({
                    name      : name,
                    hash      : hash,
                    key       : key,
                    isPublic  : isPublic,
                    size      : size,
                    mimeType  : mimeType,
                    namePrefix: namePrefix,
                    UserId    : userId
                })
                    .then(function(){
                        res.json({status: 1, info: {key: key, size: size, name: name, isPublic: isPublic}})
                    })
                    .error(function(err){
                        res.json({status: 0, info: err})
                    });


            }
        }
    }
};
