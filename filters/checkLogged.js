module.exports = function(req, res, next){
    if (req.session.logged === true)
    {
        res.locals.logged = true;
    }
    else
    {
        res.locals.logged = false;
    }

    next();
};