var uuid = require('node-uuid');
var sendEmail = require('../lib/email');
exports.controllers = {
    '/sign_in': {
        get: {
            route: function(req, res, next)
            {
                next();
            }
        },
        post: {
            route: function(req, res, next)
            {
                var username = req.body.username;
                var password = req.body.password;

                var User = req.app.get('models').User;

                User.findOne({
                    where: {
                        name: username
                    }
                })
                    .then(function(user){
                        if (user === null)
                        {
                            console.log('xxxxxxxx', res.__("user.login.user_not_exist"));
                            return res.json({
                              status: 0,
                              info: res.__("user.login.user_not_exist")
                            });
                        }
                        else if (user.password !== password)
                        {
                            console.log('xxxxxxxx', res.__("user.login.password_wrong"));
                            return res.json({
                              status: 0,
                              info: res.__("user.login.password_wrong")
                            });
                        }
                        else
                        {
                            req.session.logged = true;
                            req.session.username = user.name;
                            req.session.userId = user.id;

                            req.app.locals.logged = true;
                            req.app.locals.username = user.name;


                           return res.json({status: 1, info: 'sign in success'})
                        }

                    })
                    .error(function(err){
                        res.json({status: 0, info: err})
                    });
            }
        }
    },
    '/sign_up': {
        get: {
            route: function(req, res, next)
            {
                next();
            }
        },
        post: {
            route: function(req, res, next)
            {
                var email    = req.body.email;
                var username = req.body.username;
                var password = req.body.password;

                var User = req.app.get('models').User;

                User.create({
                    email: email,
                    name: username,
                    password: password
                })
                    .then(function(user){
                        req.session.logged = true;
                        req.session.username = user.name;
                        req.session.userId = user.id;

                        req.app.locals.logged = true;
                        req.app.locals.username = user.name;

                        return res.json({status: 1, info: 'sign up success'});
                    })
                    .error(function(err){
                        res.json({status: 0, info: err})
                    })
            }
        }
    },
    '/sign_out': {
        get: {
            route: function(req, res, next)
            {
                req.session.destroy(function(){
                    delete req.app.locals.logged;
                    delete req.app.locals.username;

                    return res.redirect('/');
                });
            }
        }
    },
    '/reset_password': {
        get: {
            route: function(req, res, next)
            {
                next();
            }
        },
        //todo: not implemented
        post: {
            route: function(req, res, next)
            {
                var email = req.body.email;
                var User = req.app.get('models').User;

                User.findOne({
                    where: {
                        email: email
                    }
                })
                .then(function(user){
                    if (user)
                    {
                        // send an email to `email address` with a link
                        var token = uuid.v4();
                        var link = 'localhost:5678/user/do_reset_password/'+token+'-'+user.name;

                        // insert the token into db, then send the email
                        user.update({
                            emailToken: token
                        })
                        .then(function(){
                            sendEmail({
                                mailTo: 'mygoare@gmail.com',
                                mailStrategy: 'resetPassword'
                            }, function(err){
                                if (err)
                                {
                                    return res.json(err);
                                }

                                console.log('link: ', link);
                                return res.render('user/email_sent_reset_password');

                            });
                        })
                    }
                    else
                    {
                        res.json({
                            status: 0,
                            info: 'email address does not exist.'
                        })
                    }
                })
                .error(function(err){
                    res.json({
                        status: 0,
                        info: err
                    })
                })
            }
        }
    },
    // 110ec58a-a0f2-4ac4-8393-c866d813b8d1
    // email link comes to here
    '/do_reset_password/:token(\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12})-:name(\\w+)': {
        get: {
            route: function(req, res, next)
            {
                var token = req.params.token;
                var name = req.params.name;

                var User = req.app.get('models').User;

                User.find({
                    where: {
                        emailToken: token,
                        name: name
                    }
                })
                .then(function(user){
                    if (user)
                    {
                        res.locals.token = token;
                        res.locals.username = name;
                        res.render('user/do_reset_password')
                    }
                    else
                    {
                        res.status(404).render('404');
                    }
                })
            }
        }
    },
    '/do_reset_password': {
        post: {
            route: function(req, res, next)
            {
                var token      = req.body.token;
                var username   = req.body.username;
                var newPwd     = req.body.newPassword;
                var confirmPwd = req.body.confirmPassword;

                if (newPwd !== confirmPwd)
                {
                    return res.json({
                        status: 0,
                        info: 'The passwords are not the same'
                    })
                }

                var User = req.app.get('models').User;

                User.findOne({
                    where: {
                        name: username,
                        emailToken: token
                    }
                })
                .then(function(user){
                    if (user)
                    {
                        user.update({
                            password: newPwd,
                            emailToken: null
                        })
                        .then(function(){
                            res.locals.info = "Password changed successfully";
                            res.render('index');
                        })
                    }
                    else
                    {
                        console.log(user);
                        res.status(404).render('404');
                    }
                })
            }
        }
    },
    '/settings': {
        get: {
            preRoutes: ['checkAuthenticated', 'checkLogged'],
            route: function(req, res, next)
            {
                next();
            }
        }
    },
    //todo: not implemented
    '/change_password': {
        post: {
            preRoutes: ['checkAuthenticated'],
            route: function (req, res, next)
            {

            }
        }
    },
    //todo: not implemented
    '/change_profile_link': {
        post: {
            preRoutes: ['checkAuthenticated'],
            route: function(req, res, next)
            {

            }
        }
    }
};
