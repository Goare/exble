var path = require('path');
var glob = require('glob');
var methods = require('methods');
var config = require('../config.js');
var gController = require('./gController.js');

function gRouter(app, opts) {

    var controllersDir = config.rootPath + '/controllers/';
    var controllersFile = glob.sync(controllersDir+'**/*.js');

    controllersFile.forEach(function(file, index){
        //console.log(file);

        file = file.replace(/\/index\.js$/, '');
        file = file.replace(/\.js$/, '');

        var instance = require(file);
        var routePrefix = path.relative(controllersDir, file);

        //app.get('/', ['checkLogin'], function(){});

        var router = instance.controllers || {};

        for (i in router)
        {
            var r = router[i];

            var p = routePrefix+i;

            methods.forEach(function(method, index){  // ['get', 'post', 'put', 'delete']
                var eachRouter = r[method];
                if (eachRouter)
                {

                    var controller = new gController(eachRouter, p);

                    //console.log(method, p, eachRouter);


                    var routePath = /^\//.test(p) ? p : '/'+p;
                    app[method](
                        routePath,
                        [].concat(controller.getPreRoutes())
                          .concat(controller.getRoute())
                          .concat(controller.getPostRoutes())
                          .concat(controller.renderView())
                    );

                    //console.log(routePath, controller.getPreRoutes(), controller.getRoute(), controller.getPostRoutes(), controller.renderView());

                }
            });
        }
    });

}

module.exports = gRouter;
