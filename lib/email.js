var request = require('request');


// Templates:

var emailTemplate = {
    // verify email address, html string
    verifyEmailAddress: {
        subject: "Verify email address",
        text: "<p>Verify email address</p>"
    },
    // reset password
    resetPassword: {
        subject: "Reset Password",
        text: "<p>Reset password</p>"
    }
};

var mailgunSecretAPIKey = 'key-92433b2f3ca07cf08e5f5d764b89a26f';
var mailgunPublicAPIKey = 'pubkey-b4837a30e6ae9cab736365610af862a1';
var mailgunUrl = 'https://api:'+mailgunSecretAPIKey+'@api.mailgun.net/v3/sandbox0f93fe2fc783427e8ce560d9df497d35.mailgun.org/messages';
var sendEmail = function(options, cb)
{
    var mailTo  = options.mailTo || null,
        mailFrom = options.mailFrom || 'Bamket <no-reply@bamket.com>',
        subject = emailTemplate[options.mailStrategy].subject,
        text    = emailTemplate[options.mailStrategy].text

    console.log('Sending email...');
    request({
        url: mailgunUrl,
        method: 'post',
        form: {
            from: mailFrom,
            to: mailTo,
            subject: subject,
            text: text
        }
    }, function(err, res, body){
        if (err)
        {
            console.log('Request mailgun server Error: ', err);
            cb && cb({
                status: 0,
                info: JSON.stringify(err)
            });
        }

        var data = JSON.parse(body);
        if (data.id && data.message)
        {
            cb && cb();
        }
        else
        {
            cb && cb({
                status: 0,
                info: 'mailgun send mail fail'
            });
        }
    })
};

// mailTo, mailFrom, mailStrategy
module.exports = sendEmail;
