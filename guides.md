js -> camel
css -> connected by dash



定义在index.js           => 这里router规则都是零散的，如 about, contact 页面
新建文件 user.js                => 这里router规则是小类的, 如 user，所有与user相关的router
新建文件夹 user/          => 这里router规则是大类，如 api（建folder）

遵守上面方式可以避免
index.js '/user'
user.js  '/'
user/index.js '/'
此类冲突


定义 controllers， 能用 user.js 文件解决的，就不要去建文件夹, 建文件夹请小心，小心重名问题（重名了定义的router可能不会生效，因为级别没有重名文件的高）


express 正则 router : '/~:id(\\d{2})/:name';   req.params.id , req.params.name 可拿值


免写 router
sequelize db 操作
开发时直接写 scss
模版 jade


多语言









next(error)

error = new Error(JSON.stringify({code: 5xx, info: 'hello world'}))
error = new Error('hello xxx')
