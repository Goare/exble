exports.controllers = {
    '/': {
        get: {
            preRoutes: ['checkAuthenticated'],
            route: function(req, res, next)
            {
                res.render('admin/index')
            }
        }
    }
}
