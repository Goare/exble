var path = require('path');
var glob = require('glob');
var methods = require('methods');
var config = require('../config.js');

function gRouter(app, opts) {

    var controllersDir = config.rootPath + '/controllers/';
    var controllersFile = glob.sync(controllersDir+'**/*.js');

    controllersFile.forEach(function(file, index){
        console.log(file);
        
        file = file.replace(/\/index\.js$/, '');
        file = file.replace(/\.js$/, '');

        var instance = require(file);
        var routePrefix = path.relative(controllersDir, file);

        //app.get('/', ['checkLogin'], function(){});

        var router = instance.controllers || {};
        var filter = instance.filters || {};
        var afterFilter = instance.afterFilters || {};

        for (i in router)
        {
            var r = router[i];
            var f = filter[i];
            var af = afterFilter[i];

            var p = routePrefix+i;

            methods.forEach(function(method, index){  // ['get', 'post', 'put', 'delete']
                var eachRouter = r[method];
                if (eachRouter)
                {
                    if (!/^\//.test(p))
                    {
                        p = '/'+p;
                    }
                    console.log(method, p, eachRouter);
                    //var controller = new gController(app, eachRouter, p, method);

                    var fFuncArr = [], afFuncArr = [];
                    if (f && f[method])
                    {
                        f[method].forEach(function(fName, index){
                            fFuncArr.push(require(path.join(config.rootPath, 'filters', fName)));
                        });
                    }
                    if (af && af[method])
                    {
                        af[method].forEach(function(afName, index){
                            afFuncArr.push(require(path.join(config.rootPath, 'filters', afName)));
                        });
                    }

                    var vp = p.replace(/^\//, '');   // render view path
                    vp = (vp === '') ? 'index' : vp;

                    app[method](p, [].concat(fFuncArr).concat(eachRouter).concat(afFuncArr).concat(function(req, res, next){res.render(vp+'.jade')}));
                }
            });
        }
    });

}

module.exports = gRouter;
