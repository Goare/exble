module.exports = function(db) {
    db.User.create({
        name: 'mygoare',
        password: 'abc',
        email: 'mygoare@gmail.com'
    }).then(function(user){
        db.Article.bulkCreate([
            {
                title: 'first article title',
                content: 'my first article content',
                category: 'first',
                tag: 'go',
                UserId: user.id
            },
            {
                title: 'second article title',
                content: 'my second article content xxx',
                category: 'first',
                tag: 'go',
                UserId: user.id
            }
        ])
    });

    db.GuestBook.create({
        username: 'mygoare',
        email: 'mygoare@gmail.com',
        message: 'I am goare, I am from China.'
    });
};