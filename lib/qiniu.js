var qiniu = require('qiniu');

var unauthenticated = function() {
    var bucketname = 'bamket-public';

    var uploadToken = function()
    {
        var putPolicy = new qiniu.rs.PutPolicy(bucketname);


        putPolicy.callbackUrl = 'http://bamket.com:3001/qiniu/callbackAction';
        putPolicy.callbackBody = 'key=$(key)&hash=$(etag)&size=$(fsize)&mimeType=$(mimeType)&filename=$(x:filename)&logged=$(x:logged)&namePrefix=$(x:namePrefix)';
        putPolicy.callbackHost = 'callbackBamket.xxx';
        //putPolicy.callbackBodyType = "application/json";
        //putPolicy.returnUrl = returnUrl;
        //putPolicy.returnBody = '{"key":"$(key)","hash":"$(etag)","size":"$(fsize)","mimeType":"$(mimeType)"}';
        //putPolicy.asyncOps = asyncOps;
        //putPolicy.expires = expires;
        putPolicy.fsizeLimit = 1024 * 1024 * 10;  // 10m

        return putPolicy.token();
    };

    // return
    return {
        uploadToken: uploadToken()
    }
};
var authenticated = function(){
    var bucketname = 'bamket-private';

    var uploadToken = function()
    {
        var putPolicy = new qiniu.rs.PutPolicy(bucketname);

        putPolicy.callbackUrl = 'http://bamket.com:3001/qiniu/callbackAction';
        putPolicy.callbackBody = 'key=$(key)&hash=$(etag)&size=$(fsize)&mimeType=$(mimeType)&filename=$(x:filename)&logged=$(x:logged)&namePrefix=$(x:namePrefix)';
        putPolicy.callbackHost = 'callbackBamket.xxx';

        return putPolicy.token();
    };

    // return
    return {
        uploadToken: uploadToken()
    }
};

var policy = module.exports =
{
    authenticated: authenticated(),
    unauthenticated: unauthenticated()
};
