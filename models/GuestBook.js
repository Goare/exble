'use strict';

module.exports = function(sequelize, DataTypes){
    var GuestBook = sequelize.define('GuestBook', {
        username: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.INTEGER
        },
        message: {
            type: DataTypes.TEXT
        }
    });
    return GuestBook;
};
