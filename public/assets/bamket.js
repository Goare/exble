$(function(){
    var path = window.location.pathname;

    function getPolicy(cb)
    {
        $.ajax({
            url: '/qiniu/getPolicy',
            method: 'post',
            success: function(data)
            {
                if (data.error)
                {
                    console.log('Errors: ', data.error);
                }
                else
                {
                    cb && cb(data);
                }

            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
            }
        });
    }
    function uploadFile(policyData)
    {
        var token      = policyData.token,
            sizeLimit  = policyData.sizeLimit,
            namePrefix = policyData.namePrefix,
            logged     = policyData.logged,
            userId     = policyData.userId;


        var files = $(this)[0].files;
        for (var i = 0; i<files.length; i++)
        {
            var file = this.files[i];

            var upload_filename = file.name;

            if (sizeLimit && file.size > sizeLimit)
            {
                console.log('非注册用户文件最大10m');
                continue;
            }

            var data = new FormData();
            data.append('file', file);
            data.append('token', token);
            data.append('key', namePrefix + '/'+upload_filename);
            // customize field
            data.append('x:filename', upload_filename);
            data.append('x:namePrefix', namePrefix);
            data.append('x:logged', logged);
            data.append('x:userId', userId);

            //todo I will append a list of elements to file lists, showing the progress bar running, when finished, remove the appended html & change the vue component's data model

            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            console.log(evt.loaded, evt.total);
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = Math.floor(percentComplete * 100);
                            console.log(percentComplete);

                            if (percentComplete === 100) {
                                console.log('upload done!');
                            }

                        }
                    }, false);

                    return xhr;
                },
                url: 'https://up.qbox.me',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR) {
                    if (data.error) {
                        // Handle errors here
                        console.log('ERRORS: ' + data.error);
                    }
                    else {
                        console.log(data);

                        // set localStorage
                        ///*
                        // random
                        // user_name
                        // * */
                        //var file_name = data.info.filename;
                        //var file_key = encodeURI(data.info.key);
                        //var file_created_timestamp = data.info.created_timestamp || null;
                        //
                        //// when logged, do not write localStorage
                        //if (data.info.logged === 'false') {
                        //    store.set('random-' + data.info.filename, {
                        //        name: file_name,
                        //        key: file_key,
                        //        created_timestamp: file_created_timestamp
                        //    });
                        //}
                        //bam.files.push({name: file_name, key: file_key});
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // Handle errors here
                    console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });

        }
        $(this).val('');
    }
    $('#bamket-upload-files-input').on('change', function(e){
        var self = this;
        e.preventDefault();

        getPolicy(uploadFile.bind(self));
    });



    /*********************************************************************************/
    // set language
    var lang = Cookies.get('lang') || 'en';
    $('#bamket-select-language').val(lang)
        .on('change', function(){
            var value = $(this).val();
            Cookies.set('lang', value);

            window.location.reload();
        });


    /*********************************************************************************/
    // sign in & sign up
    $('#bamket_sign_in_form').on('submit', function(e)
    {
        var self = this;
        e.preventDefault();

        $.ajax({
            url: '/user/sign_in',
            method: 'post',
            data: {
                username: self.username.value,
                password: self.password.value
            },
            success: function(data)
            {
                if (data.status === 1)
                {
                    var redir = location.href.match(/redir\=(.+)/);
                    location.href= redir ? redir[1] : '/';
                }
            },
            error: function(err)
            {
                console.log(err);
            }
        })
    });

    $('#bamket_sign_up_form').on('submit', function(e)
    {
        var self = this;
        e.preventDefault();

        $.ajax({
            url: '/user/sign_up',
            method: 'post',
            data: {
                email: self.email.value,
                username: self.username.value,
                password: self.password.value
            },
            success: function(data)
            {
                if (data.status === 1)
                {
                    location.href= '/';
                }
            },
            error: function(err)
            {
                console.log(err);
            }
        })
    });

    // reset password



    console.log(document.cookie);



    var getFileLists = function()
    {
        // I think I will put a vue component there(home and profile pages), and the component will do all the things listed blow for me.

        // send a request to server, logged ? get file lists, not, get file lists from localStorage
        // render vue component, bind with actions(link, set public or private, copy, delete)

        // logged ? render with action permissions, not, then only with link

        console.log('xxxxxxxx', 'I am going to generate file lists');
    };
    if (path === '/' || /\/~\w+/.test(path))
    {
        getFileLists();
    }

    // set active nav bar
    $('.nav > li > a[href="'+path+'"]').parent().addClass('active');


    // make a flash(vue component) to show res.locals.info

});
