var Promise = require('bluebird');


var add = function(){
    var sum = 0;
    var args = (arguments.length === 1) ? [arguments[0]] : Array.apply(null, arguments);
    args.forEach(function(n, i){
        sum += n;
    });
    return sum;
};
Promise.resolve('Hello').then(add.bind(null, 2)).then(console.log);


var fs = Promise.promisifyAll(require('fs'));

fs.readFileAsync('./readme.md').then(function(content){
    console.log(content.toString());
});

var a = function(){
    return new Promise(function(resolve, reject){
        require('fs').readFile('./readme.md', function(err, data){
            if (err)
            {
                reject(err)
            }
            else
            {
                resolve(data)
            }
        })
    })
};

a().then(function(data){console.log(data.toString())}).error(function(err){console.log("error: ", err.message)}).catch(function(err){console.log("Catch error:", err.message)});

var someAsyncThing = function()
{
    return new Promise(function(resolve, reject){
        resolve(x + 2);
    })
};

someAsyncThing()
.then(function(){
    console.log('everything is great');
})
.catch(function(err){
    console.log('oh no, ', err);
})







Promise.resolve([
    fs.readFileAsync('app.js'),
    fs.readFileAsync('config.js')
]).spread(function(t1, t2){
   console.log(t1, t2);
    console.log('---------------spread');    
});

Promise.resolve([
    fs.readFileAsync('app.js'),
    fs.readFileAsync('config.js')
]).all().then(function(files){
    console.log(files, files.length)
    console.log('---------------all then');    
});